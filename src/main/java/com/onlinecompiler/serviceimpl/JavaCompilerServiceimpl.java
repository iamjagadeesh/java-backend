package com.onlinecompiler.serviceimpl;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;

import com.onlinecompiler.service.JavaCompilerService;

@Service
public class JavaCompilerServiceimpl implements JavaCompilerService {

	@Override
	public File createFile() {
		// TODO Auto-generated method stub
		String dir = System.getProperty("user.dir");

		File javacompiler = new File(dir + "\\javacompiler");
		if (javacompiler.exists()) {
			System.out.println(javacompiler + " exists");

		} else {
			javacompiler.mkdir();
			System.out.println(javacompiler + "  not exists so created");
		}
		String filename = "editor.java";
		File file = new File(javacompiler + "/" + filename);
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;
	}

	@Override
	public String execute(String content) {
		RandomAccessFile raf;
		String dir = System.getProperty("user.dir");

		File javacompiler = new File(dir + "/javacompiler");
		if (javacompiler.exists()) {
			System.out.println(javacompiler + " exists");

		} else {
			javacompiler.mkdir();
			System.out.println(javacompiler + "  not exists so created");
		}
		String filename = "editor.java";
		File file = new File(javacompiler + "/" + filename);
		try {
			raf = new RandomAccessFile(file, "rw");
			raf.writeBytes(content);
			raf.close();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		String errorContent = null;
		try {
		/*	File file = createFile();
			raf = new RandomAccessFile(file, "rw");
			raf.writeBytes(content);
			raf.close();*/
			removeLogFile(javacompiler.getAbsolutePath() + "/" + "error.log");
			removeLogFile(javacompiler.getAbsolutePath() + "/" + "output.log");
			int beginIndex=content.indexOf("class");
			int endIndex=content.indexOf("{");
			String className = content.substring(beginIndex+5, endIndex).trim();
			System.out.println(className);
			ProcessBuilder builder=new ProcessBuilder("javac",file.getAbsolutePath());
			builder.redirectErrorStream(true);
			builder.redirectOutput(new File(javacompiler.getAbsolutePath()+"/"+"error.log"));
			Process process=builder.start();			
			Thread.sleep(2000);
			
			errorContent = new String(Files.readAllBytes(Paths.get(javacompiler.getAbsolutePath()+"/"+"error.log")));
			if(errorContent == null || errorContent.length() == 0) {
				System.out.println(javacompiler.getAbsolutePath());
				builder=new ProcessBuilder("java ", "-cp", javacompiler.getAbsolutePath() + "/:" , className);
				builder.redirectErrorStream(true);
				builder.redirectOutput(new File(javacompiler.getAbsolutePath()+"/"+"output.log"));
				System.out.println(builder.command());
				process=builder.start();
				Thread.sleep(2000);
				String outputContent = new String(Files.readAllBytes(Paths.get(javacompiler.getAbsolutePath()+"/"+"output.log")));	
				removeLogFile(javacompiler.getAbsolutePath() + "/" + className +".class");

				return outputContent;
			}
		} catch (IOException | InterruptedException e1) {
			e1.printStackTrace();
			errorContent = e1.getMessage();
		}
		
		return errorContent;
	}

	private void removeLogFile(String fileName) {
		File file = new File(fileName);
		if (file.exists()) {
			file.delete();
			System.out.println( fileName + "  deleted");
		}
	}
}
