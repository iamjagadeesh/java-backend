package com.onlinecompiler.serviceimpl;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;

import com.onlinecompiler.service.CppCompilerService;

@Service
public class CppCompilerServiceimpl implements CppCompilerService {

	@Override
	public File createFile() {
		String dir = System.getProperty("user.dir");
		File cppcompiler = new File(dir + "\\cppcompiler");
		if (cppcompiler.exists()) {
			System.out.println(cppcompiler + " exists");

		} else {
			cppcompiler.mkdir();
			System.out.println(cppcompiler + "  not exists so created");
		}
		String filename = "editor.java";
		File file = new File(cppcompiler + "/" + filename);
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String execute(String content) {
		RandomAccessFile raf;
		String dir = System.getProperty("user.dir");
		File cppcompiler = new File(dir + "\\cppcompiler");
		if (cppcompiler.exists()) {
			System.out.println(cppcompiler + " exists");

		} else {
			cppcompiler.mkdir();
			System.out.println(cppcompiler + "  not exists so created");
		}
		String filename = "editor.cpp";
		File file = new File(cppcompiler + "/" + filename);
		try {
			raf = new RandomAccessFile(file, "rw");
			raf.writeBytes(content);
			raf.close();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		String errorContent = null;
		try {
			/*
			 * File file = createFile(); raf = new RandomAccessFile(file, "rw");
			 * raf.writeBytes(content); raf.close();
			 */

			removeLogFile(cppcompiler.getAbsolutePath() + "/" + "error.log");
			removeLogFile(cppcompiler.getAbsolutePath() + "/" + "output.log");
			
			ProcessBuilder builder = new ProcessBuilder("g++", file.getAbsolutePath(), "-o", file.getAbsolutePath().replace(".cpp", ""));// compiling cpp file
			builder.redirectErrorStream(true);
			builder.redirectOutput(new File(cppcompiler.getAbsolutePath() + "/" + "error.log"));
			Process process = builder.start();
			Thread.sleep(2000);

			errorContent = new String(Files.readAllBytes(Paths.get(cppcompiler.getAbsolutePath() + "/" + "error.log")));
			if (errorContent == null || errorContent.length() == 0) {
				builder = new ProcessBuilder(file.getAbsolutePath().replace(".cpp", "")); // executing cpp file
				builder.redirectErrorStream(true);
				builder.redirectOutput(new File(cppcompiler.getAbsolutePath() + "/" + "output.log"));
				process = builder.start();
				Thread.sleep(2000);
				String outputContent = new String(
						Files.readAllBytes(Paths.get(cppcompiler.getAbsolutePath() + "/" + "output.log")));
				return outputContent;
			}
		} catch (IOException | InterruptedException e1) {
			e1.printStackTrace();
			errorContent = e1.getMessage();
		}
		return errorContent;
	}

	private void removeLogFile(String fileName) {
		File file = new File(fileName);
		if (file.exists()) {
			file.delete();
			System.out.println( fileName + "  deleted");
		}
	}

}
