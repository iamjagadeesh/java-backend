package com.onlinecompiler.service;

import java.io.File;

public interface JavaCompilerService {

	public File createFile();

	public String execute(String content);

}
