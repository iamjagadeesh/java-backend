package com.onlinecompiler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onlinecompiler.service.CppCompilerService;

@RestController
@CrossOrigin
public class CppController {

	@Autowired
	private CppCompilerService cppCompilerService;
	@PostMapping("/compileCpp")
	public String test(@RequestParam String content) {
		System.out.println("called" + content);
		return cppCompilerService.execute(content);
	}
}
